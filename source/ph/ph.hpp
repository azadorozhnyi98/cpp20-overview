#ifndef __PH_HPP__
#define __PH_HPP__

//------------------------------------------------------------------------------

#ifdef _MSC_VER
#pragma warning( push )

#pragma warning( disable : 4514 ) // unreferenced inline function has been removed
#pragma warning( disable : 4710 ) // function not inlined
#pragma warning( disable : 4365 ) // signed/unsigned mismatch

#pragma warning( disable : 4582 ) // constructor is not implicitly called
#pragma warning( disable : 4623 ) // default constructor was implicitly defined as deleted
#pragma warning( disable : 4625 ) // copy constructor was implicitly defined as deleted
#pragma warning( disable : 5026 ) // move constructor was implicitly defined as deleted
#pragma warning( disable : 4626 ) // copy assignment operator was implicitly defined as deleted
#pragma warning( disable : 5027 ) // move assignment operator was implicitly defined as deleted

#pragma warning( disable : 4571 ) // SEH are no longer caugth by catch ( ... )
#pragma warning( disable : 5039 ) // pointer or reference to potentially throwing function passed to extern C function
#pragma warning( disable : 4355 ) // 'this' is used in base member initializer list
#pragma warning( disable : 4774 ) // format string expected in argument is not string literal
#pragma warning( disable : 4668 ) // '<<name>>' is not defined as a preprocessor macro

#pragma warning( disable : 4121 ) // alignment of a member was sensetive to packing
#pragma warning( disable : 4820 ) // padding bytes added after data member
#pragma warning( disable : 4324 ) // structure was padded due to alignment specifier

#endif

//------------------------------------------------------------------------------

#include <cassert>
#include <string>
#include <vector>
#include <deque>
#include <iostream>
#include <memory>
#include <typeinfo>
#include <array>
#include <chrono>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <map>
#include <set>
#include <exception>
#include <functional>
#include <regex>
#include <initializer_list>
#include <type_traits>
#include <thread>
#include <future>
#include <any>
#include <variant>
#include <optional>
#include <Windows.h>
#include <concepts>
#include <ranges>
#include <compare>

//------------------------------------------------------------------------------

#ifdef _MSC_VER
#pragma warning( pop )
#endif

//------------------------------------------------------------------------------

#ifdef _MSC_VER
#pragma warning( disable : 4514 ) // unreferenced inline function has been removed
#pragma warning( disable : 4710 ) // function not inlined
#pragma warning( disable : 4555 ) // expression has no effect; expected expression with side-effect

#pragma warning( disable : 4582 ) // constructor is not implicitly called
#pragma warning( disable : 4623 ) // default constructor was implicitly defined as deleted
#pragma warning( disable : 4625 ) // copy constructor was implicitly defined as deleted
#pragma warning( disable : 5026 ) // move constructor was implicitly defined as deleted
#pragma warning( disable : 4626 ) // copy assignment operator was implicitly defined as deleted
#pragma warning( disable : 5027 ) // move assignment operator was implicitly defined as deleted

#pragma warning( disable : 4121 ) // alignment of a member was sensetive to packing
#pragma warning( disable : 4820 ) // padding bytes added after data member
#pragma warning( disable : 4324 ) // structure was padded due to alignment specifier

#pragma warning( disable : 4061 ) // enumerator in switch of enum is not explicitly handled by case label
#endif

//------------------------------------------------------------------------------

#endif // __PH_HPP__
