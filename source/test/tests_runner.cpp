#include "ph/ph.hpp"

#include "test/tests_runner.hpp"

//------------------------------------------------------------------------------

TestsRunner::TestsRunner() = default;

//------------------------------------------------------------------------------

TestsRunner::~TestsRunner() = default;

//------------------------------------------------------------------------------

TestsRunner&
TestsRunner::getInstance()
{
    static TestsRunner s_testRunner;
    return s_testRunner;
}

//------------------------------------------------------------------------------

void
TestsRunner::addTest( const std::string & _name, TestCase _testCase )
{
    m_testCases.push_back( { _name, _testCase } );
}

//------------------------------------------------------------------------------

void
TestsRunner::runTests( const std::string & _filter ) const
{
    Filter filter = buildFilter( _filter );
    TestCases testCases = filterTestCases( filter );

    if( !_filter.empty() )
        std::cout << "Filter: " << _filter << std::endl;

    std::cout << "Running " << testCases.size() << " test(s):\n";

    int counter = 1;
    for( auto& testCase : testCases )
    {
        std::cout << "Test #" << counter << " \"" << testCase.first << "\"\n";
        ( *testCase.second )( );
        ++counter;
    };

    std::cout << "Finished running tests.\n";
}

//------------------------------------------------------------------------------

TestsRunner::Filter
TestsRunner::buildFilter( const std::string & _filter ) const
{
    if( _filter.empty() )
        return []( const std::string& )
    {
        return true;
    };

    return[rule = std::regex{ _filter }]( const std::string& _testCase )
    {
        return std::regex_match( _testCase, rule );
    };
}

//------------------------------------------------------------------------------

TestsRunner::TestCases
TestsRunner::filterTestCases( Filter _filter ) const
{
    assert( !m_testCases.empty() );

    TestCases result;
    for( auto& testCase : m_testCases )
    {
        if( _filter( testCase.first ) )
            result.emplace_back( testCase );
    };

    std::sort(
        result.begin()
        , result.end()
        , [&]( const auto& _left, const auto& _right )
    {
        return _left.first < _right.first;
    }
    );

    return result;
}

//------------------------------------------------------------------------------
