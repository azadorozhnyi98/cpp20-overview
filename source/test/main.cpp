#include "ph/ph.hpp"

#include "test/tests_runner.hpp"

//------------------------------------------------------------------------------

int main( int _args, char** _argv )
{
    std::string filter;

    if( _args > 1 )
        filter = _argv[1];

    TestsRunner::getInstance().runTests( filter );
    return 0;
}
