#ifndef __TEST_CASE_HPP__
#define __TEST_CASE_HPP__

//------------------------------------------------------------------------------

#include "test/test_case_registrator.hpp"

//------------------------------------------------------------------------------

inline
void ASSERT( bool _c )
{
    if( !_c )
        std::terminate();
}

//------------------------------------------------------------------------------

#define ASSERT_THROWS( TESTED_CODE, EXPECTED_EXCEPTION )	\
	try														\
	{														\
		{ TESTED_CODE; }									\
		assert( ! "Exception must have been thrown" );		\
	}														\
	catch ( const EXPECTED_EXCEPTION & )					\
	{														\
	}														\
	catch ( const std::exception & )						\
	{														\
		assert( ! "Wrong Exception has been thrown" );		\
	}

//------------------------------------------------------------------------------

#define DECLARE_TEST_CASE( NAME )										\
	void NAME ();														\
	static TestCaseRegistrator gs_registrator_##NAME( #NAME, & NAME );	\
	void NAME ()

//------------------------------------------------------------------------------

#endif // __TEST_CASE_HPP__
