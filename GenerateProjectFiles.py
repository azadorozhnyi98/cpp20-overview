import os
import sys
import platform

sys.path.insert(1, "scripts/cmake")

from scripts.cmake.FilesListsGenerator import FilesListsGenerator
from scripts.cmake.FilterData import FilterData

#------------------------------------------------------------------------------

def generate_files_lists(filters, targets_dirs):
	generator = FilesListsGenerator()

	for filter in filters:
		generator.add_filter(filter)

	for target_dir in targets_dirs:
		generator.generate_files_list(os.path.normpath(target_dir))

#------------------------------------------------------------------------------

def generate_prj_files(build_dir):
	if not os.path.exists(build_dir):
		os.mkdir(build_dir)

	os.chdir(build_dir)
	os.system("cmake .. -A x64 -T host=x64")

#------------------------------------------------------------------------------

filters = list()
filters.append(FilterData("sources", "Sources.cmake", [".c",".cpp"]))
filters.append(FilterData("headers", "Headers.cmake", [".h",".hpp", ".inl"]))
filters.append(FilterData("ui_files", "ui_files.cmake", [".ui"]))

targets_dirs = list()
targets_dirs.append("source")

build_dir = "build"

#------------------------------------------------------------------------------

generate_files_lists(filters, targets_dirs)
generate_prj_files(build_dir)

#------------------------------------------------------------------------------