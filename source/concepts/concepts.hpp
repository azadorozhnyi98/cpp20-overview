#ifndef __CONCEPTS_HPP__
#define __CONCEPTS_HPP__

#include "ph/ph.hpp"

//------------------------------------------------------------------------------

namespace Concepts
{

//------------------------------------------------------------------------------

template<typename T>
requires std::integral<T>
    auto gcd( T a, T b )
{
    if( b == 0 ) return a;
    else return gcd( b, a % b );
}

template<typename T>
auto gcd1( T a, T b ) requires std::integral<T>
{
    if( b == 0 ) return a;
    else return gcd1( b, a % b );
}

template < std::integral T>
auto gcd2( T a, T b )
{
    if( b == 0 ) return a;
    else return gcd2( b, a % b );
}

std::integral auto gcd3( std::integral auto a, std::integral auto b ) // does not compile
{
    if( b == 0 ) return a;
    else return gcd3( b, a % b );
}

auto gcd4( auto a, auto b )
{
    if( b == 0 ) return a;
    return gcd4( b, a % b );
}

template<std::integral T1, std::integral T2>
auto gcd4( T1 a, T2 b )
{
    if( b == 0 ) return a;
    return gcd4( b, a % b );
}

template <unsigned int i>
requires ( i <= 20 )
    int sum( int j )
{
    return i + j;
}

template<std::regular T >
struct MyClass
{
};

template<typename T>
struct MyVector
{
    void pushBack( const T& ) requires std::copyable<T>
    {
    }
};

template < std::integral... Args>
bool all( Args... args )
{
    return ( ... && args );
}

template < std::integral... Args>
bool any( Args... args )
{
    return ( ... || args );
}

template < std::integral... Args>
bool none( Args... args )
{
    return not( ... || args );
}

inline std::integral auto getIntegral( int val )
{
    return val;
}

inline void overload( auto t )
{
    std::cout << "auto : " << t << '\n';
}

inline void overload( std::integral auto t )
{
    std::cout << "Integral : " << t << '\n';
}

inline void overload( long t )
{
    std::cout << "long : " << t << '\n';
}

//------------------------------------------------------------------------------

} // namespace Concepts

//------------------------------------------------------------------------------

#endif // __CONCEPTS_HPP__
