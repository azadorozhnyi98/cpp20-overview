#include "ph/ph.hpp"

#include "test/test_case_registrator.hpp"
#include "test/tests_runner.hpp"

//------------------------------------------------------------------------------

TestCaseRegistrator::TestCaseRegistrator( const std::string& _name, TestCase _testCase )
{
    TestsRunner::getInstance().addTest( _name, _testCase );
}

//------------------------------------------------------------------------------

TestCaseRegistrator::~TestCaseRegistrator() = default;

//------------------------------------------------------------------------------