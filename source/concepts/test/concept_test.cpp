#include "ph/ph.hpp"

#include "test/test_case.hpp"
#include "concepts/concepts.hpp"

//------------------------------------------------------------------------------

namespace Concepts::Test
{

DECLARE_TEST_CASE( Simple_Concepts )
{
    using namespace Concepts;
    ASSERT( gcd( 100, 10 ) == 10 );
    ASSERT( gcd1( 100, 10 ) == 10 );
    ASSERT( gcd2( 100, 10 ) == 10 );
    ASSERT( gcd3( 100, 10 ) == 10 );
    ASSERT( gcd4( 100, 10 ) == 10 );

    sum<20>( 3 );
    //sum<21>( 3 ); // ERROR

    static_assert( std::three_way_comparable<int> );

    struct NonCopyable
    {
        NonCopyable() = default;
        NonCopyable( const NonCopyable& ) = delete;
    };

    MyClass<int> a;
    //MyClass<int&> b; // ERROR because a reference is not regular
    MyVector<NonCopyable> c;
    //c.pushBack( 3 ); // ERROR

    ASSERT( all( 5, true, false ) == false );
    ASSERT( any( 5, true, false ) == true );
    ASSERT( none( 5, true, false ) == false );

    auto inegral = getIntegral( 3 );
    std::vector<int> vec{ 1, 2, 3, 4, 5 };
    for( std::integral auto& i : vec )
    {
        i *= 2;
    }

    overload( 3.14 ); // auto
    overload( 2010 ); // Integral
    overload( 2020L ); // long

    int intA = 0, intB = 0;
    static_assert( std::three_way_comparable<int> );
    ASSERT( ( intA <=> intB == 0 ) == bool( intA == intB ) );
    ASSERT( ( intA <=> intB != 0 ) == bool( intA != intB ) == true );
    ASSERT( ( ( intA <=> intB ) <=> 0 ) == ( 0 <=> ( intB <=> intA ) ) );
    ASSERT( ( ( intA <=> intB ) < 0 ) == bool( intA < intB ) == true );
    ASSERT( ( ( intA <=> intB ) > 0 ) == bool( intA > intB ) == true );
    ASSERT( ( ( intA <=> intB ) <= 0 ) == bool( intA <= intB ) == true );
    ASSERT( ( ( intA <=> intB ) >= 0 ) == bool( intA >= intB ) == true );
}

template<std::input_or_output_iterator Iter>
constexpr std::iter_difference_t<Iter> distance( Iter first, Iter last )
{
    if constexpr( std::random_access_iterator<Iter> )
        return last - first;
    else
    {
        std::iter_difference_t<Iter> result{};
        for( ; first != last; ++first )
            ++result;
        return result;
    }
}

DECLARE_TEST_CASE( Concepts2 )
{
    static constexpr auto il = { 3, 1, 4 };
    static_assert( distance( il.begin(), il.end() ) == 3 );
    static_assert( distance( il.end(), il.begin() ) == -3 );
}

template < typename T>
concept Integral = std::is_integral<T>::value;

template <typename T>
concept SignedIntegral = Integral<T> && std::is_signed<T>::value;

template <typename T>
concept UnsignedIntegral = Integral<T> && !SignedIntegral<T>;

bool IsSigned( SignedIntegral auto integ )
{
    return true;
}

bool IsSigned( UnsignedIntegral auto integ )
{
    return false;
}

template <typename T>
concept Arithmetic = std::is_integral<T>::value || std::is_floating_point<T>::value;

template<typename T>
concept Addable = requires ( T a )
{
    a + a;
};

Addable auto add( Addable auto a, Addable auto b )
{
    return a + b;
}

template<typename T>
requires requires ( T x ) // anonymous concept, avoid it
{
    x + x;
}
T add1( T a, T b )
{
    return a + b;
}

template <typename>
struct Other;

template <>
struct Other<std::vector<int>>
{
};

template<typename T>
concept TypeRequirement = requires {
    typename T::value_type;
    typename Other<T>;
};

template<typename T>
concept Equal = requires( T a, T b )
{
    { a == b } noexcept -> std::convertible_to<bool>; // compound requirement
    { a != b } noexcept -> std::convertible_to<bool>;
};

bool areEqual( Equal auto a, Equal auto b )
{
    return a == b;
}

struct WithoutEqual
{
    bool operator==( const WithoutEqual& other ) = delete;
};

struct WithoutUnequal
{
    bool operator!=( const WithoutUnequal& other ) = delete;
};

struct WithEqual
{
    bool operator==( const WithEqual& other ) noexcept( false )
    {
        return true;
    }
    bool operator!=( const WithEqual& other ) noexcept( false )
    {
        return true;
    }
};

template <typename T>
concept Ordering =
Equal<T> &&
requires( T a, T b )
{
    { a <= b } -> std::convertible_to<bool>;
    { a < b } -> std::convertible_to<bool>;
    { a > b } -> std::convertible_to<bool>;
    { a >= b } -> std::convertible_to<bool>;
};

DECLARE_TEST_CASE( UserDefinedConceptes )
{
    ASSERT( IsSigned( -5 ) == true );
    ASSERT( IsSigned( 5u ) == false );

    struct X{ 
        X operator + ( const X& );
    };
    struct Y
    {
    };
    static_assert( Addable<int> );
    static_assert( Addable<X> );
    static_assert( !Addable<Y> );

    TypeRequirement auto myVec = std::vector<int>{ 1, 2, 3 };

    ASSERT( areEqual( 1, 5 ) == false );
    //bool res = areEqual( WithoutEqual(), WithoutEqual() );
    //bool res2 = areEqual( WithoutUnequal(), WithoutUnequal() );
    //ASSERT( areEqual( WithEqual{}, WithEqual{} ) == true ); // throw exception
}

template<typename T>
struct isSemiRegular : std::integral_constant<bool,
    std::is_default_constructible<T>::value &&
    std::is_copy_constructible<T>::value &&
    std::is_copy_assignable<T>::value &&
    std::is_move_constructible<T>::value &&
    std::is_move_assignable<T>::value &&
    std::is_destructible<T>::value &&
    std::is_swappable<T>::value>
{
};
template<typename T>
concept SemiRegular = isSemiRegular<T>::value;

template<typename T>
concept Regular = Equal<T> &&
SemiRegular<T>;


}

//------------------------------------------------------------------------------
